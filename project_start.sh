#!/bin/bash
echo ""
echo ""
echo " #  _   _  _  __        ___  ___  ___   ___     __  ___   ___   ___  ___ "
echo " # | | | || |/ /  ___  | _ \| __||   \ | __|   / / | _ \ / _ \ / __||_  )"
echo " # | |_| || ' <  |___| |  _/| _| | |) || _|   / /  |  _/| (_) |\__ \ / / "
echo " #  \___/ |_|\_\       |_|  |___||___/ |_|   /_/   |_|   \___/ |___//___|"
echo " #                                                                       "
echo -e "\e[91m---------------------------------------------------------------------------"
echo "####    POS2/Projekt v1.4[25/12/2017] | TurrisGadgets ControlPanel     ####"
echo "---------------------------------------------------------------------------"
echo -e "\e[0m"


## FUNKCE ##

function connect_dongle {
	echo ""
    	echo -e "\e[0;32mVybrano: Kontrola pripojeni USB TURRIS DONGLE\e[0m"
	echo -e "\e[1;31m------------------------------\e[0m"
		python gadget_command.py "WHO AM I?"
	echo -e "\e[1;31m------------------------------\e[0m"
}

function free_slots {
	echo ""
    	echo -e "\e[0;32mVybrano: Informace o slotu v USB DONGLE\e[0m"
	echo -e "\e[1;31m------------------------------\e[0m"
	echo "Vyber slot[tvar:XX]:"
		read slot
		python gadget_command.py "GET SLOT:$slot"
	echo -e "\e[1;31m------------------------------\e[0m"
}

function registration_slot {
	echo ""
    	echo -e "\e[0;32mVybrano: Registrace periferii do USB DONGLE\e[0m"
	echo -e "\e[1;31m------------------------------\e[0m"
	echo "Vyber volny slot[tvar:XX]:"
		read slot
	echo "24-bitova adresa periferie[YYYYYYYY]:"
	read adresa
		python gadget_command.py "SET SLOT:$slot [$adresa]"

	echo -e "\e[1;31m------------------------------\e[0m"
}

function PGX_ON-PGY_ON {
	echo""
	echo -e "\e[0;32mRELE[01]:Sepnuto | RELE[02]:Sepnuto\e[0m"
		sh scripts/start-rele01-start-rele02.sh
	echo""
}

function PGX_ON-PGY_OFF {
	echo ""
	echo -e "\e[0;32mRELE[01]:Sepnuto\e[0m | \e[1;31mRELE[02]:Vypnuto\e[0m"
    		sh scripts/start-rele01-stop-rele02.sh
	echo ""
}

function PGX_OFF-PGY_ON {
	echo ""
	echo -e "\e[1;31mRELE[01]:Vypnuto\e[0m | \e[0;32mRELE[02]:Sepnuto\e[0m"
    		sh scripts/stop-rele01-start-rele02.sh
	echo ""
}

function PGX_OFF-PGY_OFF {
	echo ""
	echo -e "\e[1;31mRELE[01]:Vypnuto | RELE[02]:Vypnuto\e[0m"
    		sh scripts/stop-rele01-stop-rele02.sh
	echo ""
}

function RELE_STATUS {
	echo ""
	echo "RELE STATUS"
	echo "--- Momentalni stav ---"
		cat status/RELE-STATUS.log
	echo ""
}

function RELE_LOGS {
	echo""
		cat ./logs/RELE_LOGS.log
	echo""
}

function ALL_LOGS {
	echo ""
	echo -e "\e[0;32mProbiha sledovani periferii...\e[0m"
	echo -e "\e[1;31m--provadi se zapis do slozky logs --\e[0m"
		python gadget_echo.py
	echo ""
	echo "-----------------------------------------"
	echo "Provadi se prevod do ALL_LOGS_CZ.log ...."
		sed -f ./logs/nahradit.sed ./logs/ALL_LOGS.log > ./logs/ALL_LOGS_CZ.log
}

function LOG_ZASUVKA {
	echo ""
	echo -e "\e[0;32m Log | zasuvka AC-88\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log | grep "AC-88"
	echo ""
}

function LOG_PIR {
	echo ""
	echo -e "\e[0;32m Log | PIR JA-83P\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log | grep "JA-83P"
	echo ""
}

function LOG_MAGN {
	echo ""
	echo -e "\e[0;32m Log | Magn.senzor JA-83M\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log | grep "JA-83M"
	echo ""
}

function LOG_OVLADAC {
	echo ""
	echo -e "\e[0;32m Log | ovladac RC-86K\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log | grep "RC-86K"
	echo ""
}

function LOG_CUSTOM {
	echo ""
	echo "Vlozte datum logu, ktery hledate(dd-mm-rrrr)"
		read date
	echo "vlozte nazev periferie(napr.: RC-86K)"
		read periferie
	echo -e "\e[0;32m Log | vlastni log\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log | grep "$date\|$periferie"
	echo ""
}

function LOG_ALL {
	echo ""
	echo -e "\e[0;32m Log | cely log\e[0m"
	echo "--------------------------------------------"
		cat ./logs/ALL_LOGS_CZ.log
	echo ""
}


function BACKUP {

echo ""
	echo "Vytvarim zalohu pro dnesni datum..."
	cd scripts
	sh backup_log.sh
	
	echo " "

}


function LOG_BACKUP {
	echo ""
	echo -e "\e[0;32m Log | Vyhledavani v zaloze\e[0m"
	echo "-------------- Seznam zaloh ---------------"
		ls -l /home/control/backup_logs | awk '{print $9}'
	echo ""
	echo "-------------------------------------------"
	echo "Napiste datum zalohy, kerou chcete otevrit"
		read -p "ve formatu [dd-mm-rrrr]" date_backup

## Osetreni datumu ##

	 echo $date_backup | grep '^[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]$'
    if [ $? -eq 0 ]; then
	 echo "------- Vypis zalohy: $date_backup --------"
         cat /home/control/backup_logs/$date_backup/daily_backup.log
     else
	 echo "------- Vypis zalohy: CHYBA --------"
         echo "Datum neni validni"
     fi

	 echo "-------------- Konec zaznamu --------------"
}

function control_rele {
	echo ""
    	echo -e "\e[0;32mVybrano: Manualni ovladani RELE AC-82\e[0m"
	echo -e "\e[1;31m------------------------------\e[0m"

all_done02=0
while (( !all_done02 )); do
    options=("[RELE01]Switch ON -- [RELE02]Switch ON" "[RELE01]SWITCH ON -- [RELE02]SWITCH OFF " "[RELE01]Switch OFF -- [RELE02]Switch ON" "[RELE01]Switch OFF -- [RELE02]Switch OFF" "RELE Status" "Zpet do hlavni nabidky")
	echo -e "\e[0;32mManualni ovladani RELE AC-82\e[0m" 
	echo "Vyber moznost: "
    select opt in "${options[@]}"; do
        case $REPLY in
        	1) PGX_ON-PGY_ON; break ;;
		2) PGX_ON-PGY_OFF; break ;;
		3) PGX_OFF-PGY_ON; break ;;
		4) PGX_OFF-PGY_OFF; break ;;
		5) RELE_STATUS; break ;;
		6) all_done02=1; break ;;
            	*) echo "Spatny vstup!" ;;
        esac
    done
done

	echo "Ukonceni..."

	echo -e "\e[1;31m------------------------------\e[0m"
}


function logs {
	echo ""
    	echo -e "\e[0;32mVybrano: Logovani stavu prenosu\e[0m"
	echo -e "\e[1;31m------------------------------\e[0m"


all_done03=0
while (( !all_done03 )); do
    options=("Spustit realtime logovani" "Logovani - RELE" "Logovani - zasuvka (AC-88)" "Logovani - PIR (JA-83P)" "Logovani - MAGN.senzor (JA-83M)" "Logovani - dalkovy ovladac (RC-86K)" "Vlastni vyhledavani" "Vypis celeho logu" "Vyhledavani v zaloze" "Vynutit zalohu rucne" "Zpet do hlavni nabidky")
	echo -e "\e[0;32mLogovani\e[0m"
    	echo "Vyber moznost: "
    select opt in "${options[@]}"; do
        case $REPLY in
		1) ALL_LOGS; break ;;
        	2) RELE_LOGS; break ;;
		3) LOG_ZASUVKA; break ;;
		4) LOG_PIR; break ;;
		5) LOG_MAGN; break ;;
		6) LOG_OVLADAC; break ;;
		7) LOG_CUSTOM; break ;;
		8) LOG_ALL; break;;
		9) LOG_BACKUP; break;;
		10) BACKUP; break ;;
		11) all_done03=1; break ;;
            	*) echo "Spatny vstup!" ;;
        esac
    done
done

	echo "Ukonceni..."


	echo -e "\e[1;31m------------------------------\e[0m"
}


all_done=0
while (( !all_done )); do
    options=("Kontrola pripojeni USB TURRIS DONGLE" "Informace o slotu v USB DONGLE" "Registrace periferii do USB DONGLE" "Manualni ovladani RELE AC-82" "Logovani stavu prenosu"
 "Ukoncit")

    	echo "Vyber moznost: "
    select opt in "${options[@]}"; do
        case $REPLY in
        1) connect_dongle; break ;;
        2) free_slots; break ;;
	3) registration_slot; break ;;
	4) control_rele; break ;;
	5) logs; break ;;
	6) all_done=1; break ;;
            *) echo "Spatny vstup!" ;;


        esac
    done
done

	echo "Ukonceni..."


