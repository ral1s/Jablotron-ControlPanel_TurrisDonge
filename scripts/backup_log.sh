#!/bin/bash

foldername=$(date +%d-%m-%Y)

mkdir -p /home/control/backup_logs/"$foldername"
cat ../logs/ALL_LOGS_CZ.log | grep "$foldername" > /home/control/backup_logs/"$foldername"/daily_backup.log 
cat ../logs/RELE_LOGS.log | grep "$foldername" >> /home/control/backup_logs/"$foldername"/daily_backup.log

echo "$foldername | Zaloha byla uspesne vytvorena a pridana do seznamu zaloh..."
