"""
Simple demo of reading messages from the TURRIS:DONGLE.

Just run it from the command line and watch the messages printed
on the standard output.
"""

from __future__ import print_function
import sys
import datetime

from device import Device



if __name__ == "__main__":
    if len(sys.argv) <= 1:
        device_name = "/dev/ttyUSB0"
    else:
        device_name = sys.argv[1]



   
    print("Using '{0}' as input device".format(device_name), file=sys.stderr)
    file = open('logs/ALL_LOGS.log', 'a')
    device = Device(device=device_name)
    reader = device.gen_lines()
    while True:
        line = reader.next()
        file.write(line)
	date = datetime.datetime.now().strftime(" %d-%m-%Y %H:%M:%S")
        file.write(date)
	file.write("\n")
        print(date, line)
        

